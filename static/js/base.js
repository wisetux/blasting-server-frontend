$(document).ready(function () {
  // Function to update selected option and its color
  function updateSelectedOption(selectedText, textColor) {
    $("#selected-option").text(selectedText).css("color", textColor);

    // Save the selected option in localStorage
    localStorage.setItem("selectedOption", selectedText);
  }

  // Get the selected option from localStorage on page load
  var savedOption = localStorage.getItem("selectedOption");
  if (savedOption) {
    updateSelectedOption(
      savedOption,
      savedOption === "RMA" ? "green" : savedOption.toLowerCase()
    );
  } else {
    // Default option on first visit
    updateSelectedOption("Blue", "blue");
  }

  // Handle click on dropdown items
  $("#blue-option").click(function () {
    updateSelectedOption($(this).text(), "blue");
  });

  $("#red-option").click(function () {
    updateSelectedOption($(this).text(), "red");
  });

  $("#rma-option").click(function () {
    updateSelectedOption($(this).text(), "green");
  });
});
